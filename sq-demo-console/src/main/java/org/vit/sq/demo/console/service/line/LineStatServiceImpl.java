package org.vit.sq.demo.console.service.line;

import org.springframework.stereotype.Component;
import org.vit.sq.demo.console.repository.LineStatInfo;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.stream.Collectors;

@Component
public class LineStatServiceImpl implements LineStatService {


    @Override
    public LineStatInfo collectLineStatistic(String line) {
        IntSummaryStatistics lineStatistics = Arrays.stream(line.split(LineStatService.SPACE_DELIMETER))
                .filter(s -> !LineStatService.EMPTY.equals(s))
                .collect(Collectors.summarizingInt(String::length));

        return new LineStatInfo(line.length(), lineStatistics.getMin(), lineStatistics.getMax(), lineStatistics.getAverage());
    }
}
