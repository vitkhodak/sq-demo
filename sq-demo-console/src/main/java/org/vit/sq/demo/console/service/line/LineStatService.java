package org.vit.sq.demo.console.service.line;

import org.vit.sq.demo.console.repository.LineStatInfo;

public interface LineStatService {

    String SPACE_DELIMETER = " ";
    String EMPTY = "";


    LineStatInfo collectLineStatistic(String line);
}
