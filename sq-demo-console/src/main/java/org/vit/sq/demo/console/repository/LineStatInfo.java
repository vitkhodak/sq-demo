package org.vit.sq.demo.console.repository;

import java.util.Objects;

public final class LineStatInfo {

    private final Integer wordInLineMinLength;
    private final Integer lineLength;
    private final Integer wordInLineMaxLength;
    private final Double wordInLineAverageLength;

    public LineStatInfo(Integer lineLength, Integer wordInLineMinLength, Integer wordInLineMaxLength, Double wordInLineAverageLength) {
        this.lineLength = lineLength;

        if (Integer.MAX_VALUE == wordInLineMinLength) {
            wordInLineMinLength = 0;
        }
        this.wordInLineMinLength = wordInLineMinLength;

        if (Integer.MIN_VALUE == wordInLineMaxLength) {
            wordInLineMaxLength = 0;
        }
        this.wordInLineMaxLength = wordInLineMaxLength;

        this.wordInLineAverageLength = wordInLineAverageLength;
    }

    public Integer getWordInLineMinLength() {
        return wordInLineMinLength;
    }

    public Integer getLineLength() {
        return lineLength;
    }

    public Integer getWordInLineMaxLength() {
        return wordInLineMaxLength;
    }

    public Double getWordInLineAverageLength() {
        return wordInLineAverageLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LineStatInfo that = (LineStatInfo) o;
        return Objects.equals(getWordInLineMinLength(), that.getWordInLineMinLength()) &&
                Objects.equals(getLineLength(), that.getLineLength()) &&
                Objects.equals(getWordInLineMaxLength(), that.getWordInLineMaxLength()) &&
                Objects.equals(getWordInLineAverageLength(), that.getWordInLineAverageLength());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getWordInLineMinLength(), getLineLength(), getWordInLineMaxLength(), getWordInLineAverageLength());
    }

    @Override
    public String toString() {
        return String.format("LineStatInfo{length: %d; min: %d; max: %d; avg: %f}", getLineLength(), getWordInLineMinLength(), getWordInLineMaxLength(), getWordInLineAverageLength());
    }
}
