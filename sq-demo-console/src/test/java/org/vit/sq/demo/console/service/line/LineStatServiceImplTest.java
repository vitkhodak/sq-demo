package org.vit.sq.demo.console.service.line;

import org.junit.Test;
import org.vit.sq.demo.console.repository.LineStatInfo;


public class LineStatServiceImplTest extends AbstractLineStatServiceTest {

    private final LineStatService lineStatService = new LineStatServiceImpl();


    @Test
    public void shouldReturnLineStatWhenEmptyLine() {
        LineStatInfo actualLineStatInfo = lineStatService.collectLineStatistic(LineStatService.EMPTY);
        LineStatInfo expectedLineStatInfo = new LineStatInfo(0, 0, 0, new Double(0));
        checkResults(actualLineStatInfo, expectedLineStatInfo);
    }

    @Test
    public void shouldReturnLineStatWhenOnlySpacesInLine() {
        LineStatInfo actualLineStatInfo = lineStatService.collectLineStatistic(SPACES_ONLY_LINE);
        LineStatInfo expectedLineStatInfo = new LineStatInfo(SPACES_ONLY_LINE.length(), 0, 0, new Double(0));
        checkResults(actualLineStatInfo, expectedLineStatInfo);
    }

    @Test
    public void shouldProcessLine() {
        LineStatInfo actualLineStatInfo1 = lineStatService.collectLineStatistic(ONE_WORD_LINE);
        LineStatInfo expectedLineStatInfo1 = new LineStatInfo(ONE_WORD_LINE.length(), ONE_WORD_LINE.length(), ONE_WORD_LINE.length(), new Double(ONE_WORD_LINE.length()));
        checkResults(actualLineStatInfo1, expectedLineStatInfo1);

        LineStatInfo actualLineStatInfo2 = lineStatService.collectLineStatistic(TEST_LINE);
        LineStatInfo expectedLineStatInfo2 = new LineStatInfo(TEST_LINE.length(), SHORTEST_WORD.length(), LONGEST_WORD.length(),
                new Double((ONE_WORD_LINE.length() + LONGEST_WORD.length() + SHORTEST_WORD.length() + ANOTHER_WORD_LINE.length()) / 4));
        checkResults(actualLineStatInfo2, expectedLineStatInfo2);
    }


}