package org.vit.sq.demo.console.service.line;

import org.vit.sq.demo.console.repository.LineStatInfo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public abstract class AbstractLineStatServiceTest {

    protected static final String SPACES_ONLY_LINE = "     ";
    protected static final String ONE_WORD_LINE = "TEST";
    protected static final String SHORTEST_WORD = "TST";
    protected static final String LONGEST_WORD = "TESTTEST";
    protected static final String ANOTHER_WORD_LINE = "TEST1";
    protected static final String TEST_LINE = "  " + ONE_WORD_LINE + "   " + LONGEST_WORD + " " + SHORTEST_WORD + "    " + ANOTHER_WORD_LINE + "  ";

    protected void checkResults(LineStatInfo actual, LineStatInfo expedcted) {
        assertThat(actual, notNullValue());
        assertThat(actual, samePropertyValuesAs(expedcted));
    }
}
